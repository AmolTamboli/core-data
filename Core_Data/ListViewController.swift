//
//  ListViewController.swift
//  Core_Data
//
//  Created by Amol Tamboli on 23/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit
protocol DataPass {
    func data (object:[String:String], index:Int, isEdit:Bool)
}

class ListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var employee = [Employee]()
    var delegate:DataPass!
    @IBOutlet weak var tbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        employee = DatabaseHelper.shareInstance.getEmployeeData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employee.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.emp = employee[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            employee = DatabaseHelper.shareInstance.deleteData(index: indexPath.row)
            self.tbl.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = ["name":employee[indexPath.row].name,"address":employee[indexPath.row].address,"city":employee[indexPath.row].city,"mobile":employee[indexPath.row].mobile]
        delegate.data(object: dict as! [String:String], index: indexPath.row, isEdit: true)
        self.navigationController?.popViewController(animated: true)
    }

}
