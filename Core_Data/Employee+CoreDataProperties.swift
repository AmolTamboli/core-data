//
//  Employee+CoreDataProperties.swift
//  Core_Data
//
//  Created by Amol Tamboli on 23/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//
//

import Foundation
import CoreData


extension Employee {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Employee> {
        return NSFetchRequest<Employee>(entityName: "Employee")
    }

    @NSManaged public var name: String?
    @NSManaged public var address: String?
    @NSManaged public var city: String?
    @NSManaged public var mobile: String?

}
