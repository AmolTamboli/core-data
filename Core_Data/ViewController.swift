//
//  ViewController.swift
//  Core_Data
//
//  Created by Amol Tamboli on 23/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController,DataPass {

    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtMobile: UITextField!
    
    var i = Int()
    var isUpdate = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnSubmit(_ sender: Any) {
        let dict = ["name":txtName.text,"address":txtAddress.text,"city":txtCity.text,"mobile":txtMobile.text]
        if isUpdate{
            DatabaseHelper.shareInstance.editData(object: dict as! [String:String], i: i)
        } else {
            DatabaseHelper.shareInstance.save(object: dict as! [String:String])
        }
    }
    
    @IBAction func show(_ sender: Any) {
        let listVC = self.storyboard?.instantiateViewController(identifier: "ListViewController") as! ListViewController
        listVC.delegate = self
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    
    func data(object: [String : String], index: Int, isEdit: Bool) {
        txtName.text = object["name"]
        txtAddress.text = object["address"]
        txtCity.text = object["city"]
        txtMobile.text = object["mobile"]
        i = index
        isUpdate = isEdit
    }
    
}

