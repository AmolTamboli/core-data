//
//  TableViewCell.swift
//  Core_Data
//
//  Created by Amol Tamboli on 23/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    var emp:Employee!{
        didSet{
            lblName.text = emp.name
            lblAddress.text = emp.address
            lblCity.text = emp.city
            lblMobile.text = emp.mobile
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
