//
//  DatabaseHelper.swift
//  Core_Data
//
//  Created by Amol Tamboli on 23/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DatabaseHelper{
    
    static var shareInstance = DatabaseHelper()
    
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
    //MARK:- SAVE DATA
    func save(object:[String:String]){
        let Employee = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context!) as! Employee
        Employee.name = object["name"]
        Employee.address = object["address"]
        Employee.city = object["city"]
        Employee.mobile = object["mobile"]
        do{
            try context?.save()
        } catch {
            print("Data Can't Save")
        }
    }
    
    //MARK:- GET DATA
    func getEmployeeData() ->[Employee]{
        var employee = [Employee]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Employee")
        do{
            employee = try context?.fetch(fetchRequest) as! [Employee]
        } catch {
                print("Can't get employee data")
        }
        return employee
    }
    
    //MARK:- DELETE DATA
    func deleteData(index:Int) ->[Employee]{
        var employee = getEmployeeData()
        context?.delete(employee[index])
        employee.remove(at: index)
        do{
            try context?.save()
        } catch {
            print("Can't delete employee data")
        }
        return employee
    }
    
    //MARK:- FUNCTION EDIT DATA
    func editData (object:[String:String], i:Int){
        let employee = getEmployeeData()
        employee[i].name = object["name"]
        employee[i].address = object["address"]
        employee[i].city = object["city"]
        employee[i].mobile = object["mobile"]
        do{
            try context?.save()
        } catch {
            print("Can't employee data is not edit")
        }
    }
}
